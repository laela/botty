package botty

import (
	"fmt"
)

type ConsoleReporter struct {
}

func (r ConsoleReporter) AddEvent(bot *Bot, event string, message string) (err error) {
	fmt.Printf("%s %s\n", event, message)

	return
}
