package reporters

import (
	"time"
	"fmt"
	"os"
	"html/template"
	"crypto/md5"
	"io"
)

type event struct {
	Message string
	Image   string
	Time    time.Time
	Id      string
}

func newEvent(message string, image string) (event, error) {
	time_now := time.Now()
	hash := md5.New()

	_, err := io.WriteString(hash, fmt.Sprintf("%v%v%v", message, image, time_now.String()))
	if (err != nil) {
		return event{}, err
	}

	event := event{
		Message: message,
		Image:   image,
		Time:    time_now,
		Id:      fmt.Sprintf("%x", hash.Sum(nil)),
	}

	return event, nil
}

const report_template = `
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Report</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  body {
      position: relative;
  }
  ul.nav-pills {
      top: 20px;
      position: fixed;
  }

  </style>
</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="20">

<div class="container-fluid">
  <div class="row">
    <nav class="col-sm-6" id="myScrollspy">
      <ul class="nav nav-pills nav-stacked">
        {{ range .}}
          <li><a href="#{{ .Id }}">{{ if .Image }}<span class="glyphicon glyphicon-picture" aria-hidden="true"></span> {{ else }}   {{ end }}{{ .Message }}</a></li>
        {{ end}}
      </ul>
    </nav>
    <div class="col-sm-6">
      {{ range .}}
          <div id="{{ .Id }}" class="thumbnail">
          	<p>{{ .Time }}</p>
            <h1>{{ .Message }}</h1>
            {{ if .Image }}
              <img src="{{ .Image }}">
            {{ end }}
          </div>
      {{ end}}
    </div>
  </div>
</div>

</body>
</html>
`

type Reporter struct {
	path   string
	events []event
}

func NewReporter(path string) (*Reporter) {
	path = fmt.Sprintf("%v/%v", path, time.Now().Unix())

	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		os.Mkdir(path, 0755)
	}

	return &Reporter{
		path: path,
	}
}

func (reporter *Reporter) AddEvent(message string, image string) (error) {
	//if (image != "") {
	//	err := exec.Command("cp", "-rf", image, reporter.path).Run()
	//	if (err != nil) {
	//		return err
	//	}
	//
	//	image = path.Base(image)
	//}

	event, err := newEvent(message, image)
	if (err != nil) {
		return err
	}

	reporter.events = append(reporter.events, event)

	return nil
}

func (reporter *Reporter) GenerateReport() (error) {
	tmpl, err := template.New("report").Parse(report_template)
	if (err != nil) {
		return err
	}

	report_path := fmt.Sprintf("%v/%v", reporter.path, "report.html")

	file, err := os.Create(report_path)
	if (err != nil) {
		return err
	}

	err = tmpl.Execute(file, reporter.events)
	if (err != nil) {
		return err
	}

	return nil
}


func dump() {
	//log.Print(message)
	//image_path := fmt.Sprintf("/tmp/%v.jpg", time.Now().UnixNano())
	//
	//err = b.Browser.Screenshot(image_path)
	//if err != nil {
	//	return
	//}

	//func (b *Bot) HighlightElementByQuery(query string, color string) (error) {
	//	command := fmt.Sprintf("document.querySelector(\"%v\").style.border=\"5px solid %v\"", query, color)
	//
	//	log.Print(command)
	//	return b.Browser.RunScript(command, nil, nil)
	//}
	//
	//func (b *Bot) HighlightElementByXPath(xpath string, color string) (error) {
	//	command := fmt.Sprintf("document.evaluate(\"%v\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.style.border =\"5px solid %v\";", xpath, color)
	//
	//	log.Print(command)
	//	return b.Browser.RunScript(command, nil, nil)
	//}
}
