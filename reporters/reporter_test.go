// +build unit

package reporters

import (
	"testing"
	"log"
	"os"
	"io"
	"net/http"
)

var test_reporter = NewReporter("/go/src/github.com/21stio/go-twitter-bot/reports")

func TestReporter_GenerateReport(testing *testing.T) {
	img, _ := os.Create("/tmp/image.jpg")
	defer img.Close()

	resp, _ := http.Get("http://i.imgur.com/Dz2r9lk.jpg")
	defer resp.Body.Close()

	_, err := io.Copy(img, resp.Body)
	if(err != nil) {
		testing.Error(err)
		log.Print(err)
	}

	test_reporter.AddEvent("hi", "/tmp/image.jpg")
	test_reporter.AddEvent("ho", "/tmp/image.jpg")
	test_reporter.AddEvent("ha", "/tmp/image.jpg")

	err = test_reporter.GenerateReport()
	if(err != nil) {
		testing.Error(err)
		log.Print(err)
	}
}