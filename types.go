package botty

import (
	"net/http"
	"errors"
)

type CookiesNotFound struct {
}

func (c *CookiesNotFound) Error() string {
	return "cookies not found"
}

type Store interface {
	SetCookies(string, []*http.Cookie) (error)
	GetCookies(string) ([]*http.Cookie, error)
}

type Reporter interface {
	AddEvent(*Bot, string, string) (error)
}

type Selector struct {
	Query  string
	Button string
	Label  string
	Link   string
	XPath  string
}

func (s Selector) IsValid() (err error) {
	c := 0
	if s.Query != "" {
		c += 1
	}
	if s.Button != "" {
		c += 1
	}
	if s.Label != "" {
		c += 1
	}
	if s.Link != "" {
		c += 1
	}
	if s.XPath != "" {
		c += 1
	}

	if c == 1 {
		return
	}

	return errors.New("invalid selector")
}

type Classifier interface {
	Classify(Bot) (Labels, error)
}

type Labels struct {
	labels []string
}

func NewLabels(labels []string) (Labels) {
	return Labels{labels: labels}
}

func (l *Labels) Add(...string) {

}

func (l Labels) Has(...string) bool {
	return true
}

func (l Labels) HasNot(...string) bool {
	return true
}
