package botty

import (
	"github.com/sclevine/agouti"
	"net/http"
	"fmt"
	"time"
	"strings"
	"errors"
	"net/url"
	"github.com/PuerkitoBio/goquery"
	"bytes"
)

const EVENT_REPORT = "REPORT"
const EVENT_ERROR = "ERROR"
const EVENT_GET_XPATH_ELEMENT = "GET_XPATH_ELEMENT"
const EVENT_GET_QUERY_ELEMENT = "GET_QUERY_ELEMENT"
const EVENT_CLICK = "CLICK"

func NewBrowser(hubUrl string, kind string) (browser *agouti.Page, err error) {
	capabilities := agouti.NewCapabilities().Browser(kind)

	browser, err = agouti.NewPage(hubUrl, agouti.Desired(capabilities))
	if err != nil {
		return
	}

	return
}

type Bot struct {
	Browser    *agouti.Page
	reporter   Reporter
	classifier Classifier
	store      Store
}

func NewBot(browser *agouti.Page, reporter Reporter, classifier Classifier, store Store) (Bot) {
	return Bot{
		Browser:    browser,
		reporter:   reporter,
		classifier: classifier,
		store:      store,
	}
}

func (b *Bot) WaitForElement(selector Selector, timeout time.Duration, interval time.Duration) (err error) {
	start := time.Now()

	var element *agouti.Selection
	for true {
		element, err = b.GetElement(selector)

		if element != nil {
			return
		}

		if time.Now().Sub(start) > timeout {
			return errors.New("WaitForElement timed out")
		}

		time.Sleep(interval)
	}

	return
}

func (b *Bot) WaitForLabels(f func(Labels) (bool), timeout time.Duration, interval time.Duration) (err error) {
	start := time.Now()

	var labels Labels
	for true {
		labels, err = b.Classify()
		if err != nil {
			return
		}

		if f(labels) {
			return
		}

		if time.Now().Sub(start) > timeout {
			return errors.New("WaitForElement timed out")
		}

		time.Sleep(interval)
	}

	return
}

func (b *Bot) SaveCookies(key string) (err error) {
	cookies, err := b.Browser.GetCookies()
	if err != nil {
		return
	}

	err = b.store.SetCookies(key, cookies)
	if err != nil {
		return
	}

	return
}

func (b *Bot) LoadCookies(key string, url string) (err error) {
	cookies, err := b.store.GetCookies(key)
	if err != nil {
		return
	}

	err = b.SetCookies(url, cookies)
	if err != nil {
		return
	}

	return
}

func (b *Bot) Report(message string) (err error) {
	err = b.reporter.AddEvent(b, EVENT_REPORT, message)
	if err != nil {
		return
	}

	return
}

func (b *Bot) Close() (err error) {
	err = b.Browser.Session().Delete()

	return
}

func (b *Bot) ReportError(message string) (err error) {
	err = b.reporter.AddEvent(b, EVENT_ERROR, message)
	if err != nil {
		return
	}

	return
}

func (b *Bot) SetCookies(url string, cookies []*http.Cookie) (err error) {
	err = b.Browser.Navigate(url)
	if err != nil {
		return
	}

	for _, cookie := range (cookies) {
		err = b.Browser.SetCookie(cookie)
		if err != nil {
			return
		}
	}

	return
}

func (b *Bot) DownloadFile(urlString string) (err error) {
	u, err := url.Parse(urlString)
	if err != nil {
		return
	}

	script := downloadjs

	err = b.Browser.Navigate(u.Host)
	if err != nil {
		return
	}

	err = b.Browser.RunScript(script, nil, nil)
	if err != nil {
		return
	}

	err = b.Browser.RunScript(fmt.Sprint("download(\"%s\")", u.String()), nil, nil)
	if err != nil {
		return
	}

	return nil
}

func (b Bot) Classify() (labels Labels, err error) {
	labels, err = b.classifier.Classify(b)
	if err != nil {
		return
	}

	return
}

func (b Bot) GetHtml() (html string, err error) {
	return b.Browser.HTML()
}

func (b Bot) GetUrl() (html string, err error) {
	return b.Browser.URL()
}

func (b Bot) GetDocument() (doc *goquery.Document, err error) {
	html, err := b.Browser.HTML()
	if err != nil {
		return
	}

	doc, err = goquery.NewDocumentFromReader(bytes.NewBuffer([]byte(html)))
	if err != nil {
		return
	}

	return
}

func (b Bot) HasSelector(selector Selector) (has bool, err error) {
	err = selector.IsValid()
	if err != nil {
		return
	}

	var doc *goquery.Document
	if selector.Query != "" {
		doc, err = b.GetDocument()
		if err != nil {
			return
		}

		has = doc.Find(selector.Query) != nil

		return
	}

	return
}

func (b *Bot) Click(selector Selector) (err error) {
	element, err := b.GetElement(selector)
	if err != nil {
		return
	}

	err = element.Click()
	if err != nil {
		return
	}

	return nil
}

func (b *Bot) Fill(selector Selector, value string) (err error) {
	element, err := b.GetElement(selector)
	if err != nil {
		return
	}

	err = element.Fill(value)
	if err != nil {
		return
	}

	return
}

func (b *Bot) GetAttribute(selector Selector, attribute_name string) (value string, err error) {
	element, err := b.GetElement(selector)
	if err != nil {
		return
	}

	value, err = element.Attribute(attribute_name)
	if err != nil {
		return
	}

	return
}

func (b *Bot) UploadFile(selector Selector, filename string) (error) {
	fileInput, err := b.GetElement(selector)
	if err != nil {
		return err
	}

	err = fileInput.UploadFile(filename)
	if err != nil {
		return err
	}

	return nil
}

func (b *Bot) GetCookies() ([]*http.Cookie, error) {
	return b.Browser.GetCookies()
}

func (b *Bot) GetElement(selector Selector) (element *agouti.Selection, err error) {
	if selector.Query != "" {
		element, err = b.getElementByQuery(selector.Query)
		if err != nil {
			return
		}
	}

	if selector.XPath != "" {
		element, err = b.getElementByXPath(selector.XPath)
		if err != nil {
			return
		}
	}

	return
}

func (b *Bot) SendKeys(keys string) (err error) {
	keystrokes := map[string][]string{"value": strings.Split(keys, "")}

	err = b.Browser.Session().Send("keys", "POST", keystrokes, nil)
	if err != nil {
		return
	}

	return
}

func (b *Bot) getElementByXPath(xpath string) (element *agouti.Selection, err error) {
	err = b.reporter.AddEvent(b, EVENT_GET_XPATH_ELEMENT, xpath)
	if err != nil {
		return
	}

	element = b.Browser.FindByXPath(xpath)

	return
}

func (b *Bot) getElementByQuery(query string) (element *agouti.Selection, err error) {
	err = b.reporter.AddEvent(b, EVENT_GET_QUERY_ELEMENT, query)
	if err != nil {
		return
	}

	element = b.Browser.Find(query)

	return
}
