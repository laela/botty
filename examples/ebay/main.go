package main

import (
	"gitlab.com/laela/botty"
	"github.com/sclevine/agouti"
	"log"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/davecgh/go-spew/spew"
	"os"
)

const URL_SIGNIN = "https://signin.ebay.de/"
const URL_ORDERS = "https://www.ebay.de/myb/PurchaseHistory?ipp=100&Period=%v"

var SIGNIN_EMAIL_INPUT = botty.Selector{Query:"#userid"}
var SIGNIN_PASS_INPUT = botty.Selector{Query:"#pass"}
var SIGNIN_SUBMIT_BTN = botty.Selector{Query:"#sgnBt"}

type Classifier struct {
}

func (c Classifier) Classify(bot botty.Bot) (labels botty.Labels, err error) {
	return
}

type EbayBot struct {
	botty.Bot
}

func NewEbayBot(browser *agouti.Page, reporter botty.Reporter) (EbayBot) {
	return EbayBot{
		Bot: botty.NewBot(browser, reporter, Classifier{}, botty.NewBoltdbStore("a", "v")),
	}
}

func (b EbayBot) Login(email string, password string) (err error) {
	err = b.Browser.Navigate(URL_SIGNIN)
	if err != nil {
		return
	}

	err = b.Fill(SIGNIN_EMAIL_INPUT, email)
	if err != nil {
		return
	}

	err = b.Fill(SIGNIN_PASS_INPUT, password)
	if err != nil {
		return
	}

	err = b.Click(SIGNIN_SUBMIT_BTN)
	if err != nil {
		return
	}

	return
}

type Order struct {
	Date string
	Name string
	Price string
}

func (b EbayBot) GetAllOrders() (orders []Order, err error) {
	orders = []Order{}

	var o []Order
	for _, page := range []int{2,3,4} {
		o, err = b.GetOrders(page)
		if err != nil {
			return
		}

		orders = append(orders, o...)
	}

	return
}


func (b EbayBot) GetOrders(page int) (orders []Order, err error) {
	orders = []Order{}

	err = b.Browser.Navigate(fmt.Sprintf(URL_ORDERS, page))
	if err != nil {
		return
	}

	doc, err := b.GetDocument()


	doc.Find("#orders .order-r").Each(func(i int, selection *goquery.Selection) {
		order := Order{}

		order.Date = selection.Find(".row-date").Text()
		order.Price = selection.Find(".purchase-info .cost-label").Text()
		order.Name = selection.Find(".vip.item-title").Text()

		orders = append(orders, order)
	})

	return
}


func main() {
	err := run()
	if err != nil {
		log.Fatal(err)
	}
}

func run() (err error) {
	reporter := botty.ConsoleReporter{}

	browser, err := botty.NewBrowser("http://192.168.99.100:4444/wd/hub", "chrome")
	if err != nil {
		return
	}

	bot := NewEbayBot(browser, reporter)
	defer bot.Close()

	err = bot.Login(os.Getenv("EMAIL"), os.Getenv("PASSWORD"))
	if err != nil {
		return
	}

	orders, err := bot.GetAllOrders()
	if err != nil {
		return
	}

	spew.Dump(orders)

	err = bot.Browser.Screenshot("/tmp/screenshot.jpg")
	if err != nil {
		return
	}

	return
}
