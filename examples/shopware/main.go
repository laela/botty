package main

import (
	"gitlab.com/laela/botty"
	"time"
	"github.com/sclevine/agouti"
	"log"
	"github.com/icrowley/fake"
)

const URL_HOME = "https://www.shopwaredemo.de/"
const URL_ACCOUNT = "https://www.shopwaredemo.de/account"

var SELECTOR_GENERAL_CLOSE_BTN = botty.Selector{Query: ".close-btn"}

var SELECTOR_NAV_ACCOUNT_BTN = botty.Selector{Query: ".btn.is--icon-left.entry--link.account--link",}
var SELECTOR_NAV_SIGNIN_BTN = botty.Selector{Query: ".blocked--link.btn.is--primary.navigation--signin-btn"}

var SELECTOR_REGISTER_KIND_SELECT = botty.Selector{Query: "#register_personal_customer_type"}
var SELECTOR_REGISTER_SALUTATION_SELECT = botty.Selector{Query: "#salutation"}
var SELECTOR_REGISTER_FIRSTNAME_INPUT = botty.Selector{Query: "#firstname"}
var SELECTOR_REGISTER_LASTNAME_INPUT = botty.Selector{Query: "#lastname"}
var SELECTOR_REGISTER_EMAIL_INPUT = botty.Selector{Query: "#register_personal_email"}
var SELECTOR_REGISTER_PASSWORD_INPUT = botty.Selector{Query: "#register_personal_password"}
var SELECTOR_REGISTER_STREET_INPUT = botty.Selector{Query: "#street"}
var SELECTOR_REGISTER_ZIPCODE_INPUT = botty.Selector{Query: "#zipcode"}
var SELECTOR_REGISTER_CITY_INPUT = botty.Selector{Query: "#city"}
var SELECTOR_REGISTER_COUNTRY_SELECT = botty.Selector{Query: "#country"}
var SELECTOR_REGISTER_REGISTER_BTN = botty.Selector{Query: ".register--submit.btn.is--primary.is--large.is--icon-right"}

const PAGE_REGISTER = "PAGE_REGISTER"
const PAGE_ACCOUNT = "PAGE_ACCOUNT"

type Classifier struct {
}

func (c Classifier) Classify(bot botty.Bot) (labels botty.Labels, err error) {
	has1, err := bot.HasSelector(SELECTOR_REGISTER_KIND_SELECT)
	if err != nil {
		return
	}

	has2, err := bot.HasSelector(SELECTOR_REGISTER_KIND_SELECT)
	if err != nil {
		return
	}

	if has1 && has2 {
		labels.Add(PAGE_REGISTER)
	}

	return
}

type ShopwareBot struct {
	botty.Bot
}

type RegistrationData struct {
	FirstName string
	LastName  string
	Email     string
	Password  string
	Street    string
	ZipCode   string
	City      string
	Country   string
}

func NewShopwareBot(browser *agouti.Page, reporter botty.Reporter) (ShopwareBot) {
	return ShopwareBot{
		Bot: botty.NewBot(browser, reporter, Classifier{}),
	}
}

func (b *ShopwareBot) GoToShop() (err error) {
	err = b.Browser.Navigate(URL_HOME)
	if err != nil {
		return
	}

	err = b.WaitForElement(SELECTOR_NAV_ACCOUNT_BTN, 5*time.Second, 100*time.Millisecond)
	if err != nil {
		return
	}

	return
}

func (b *ShopwareBot) Register(data RegistrationData) (err error) {
	//err = b.Click(SELECTOR_NAV_ACCOUNT_BTN)
	//if err != nil {
	//	return
	//}
	//
	//time.Sleep(500 * time.Millisecond)
	//
	//err = b.Click(SELECTOR_NAV_SIGNIN_BTN)
	//if err != nil {
	//	return
	//}

	err = b.Browser.Navigate(URL_ACCOUNT)
	if err != nil {
		return
	}

	err = b.Click(botty.Selector{Query: ".close-btn"})
	if err != nil {
		return
	}

	//err = b.WaitForLabels(func(labels botty.Labels) bool {
	//	return labels.Has(PAGE_REGISTER)
	//}, 2*time.Second, 100*time.Millisecond)
	//if err != nil {
	//	return
	//}

	err = b.Fill(SELECTOR_REGISTER_KIND_SELECT, "Privatkunde")
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_SALUTATION_SELECT, "Herr")
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_FIRSTNAME_INPUT, data.FirstName)
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_LASTNAME_INPUT, data.LastName)
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_EMAIL_INPUT, data.Email)
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_PASSWORD_INPUT, data.Password)
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_STREET_INPUT, data.Street)
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_ZIPCODE_INPUT, data.ZipCode)
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_CITY_INPUT, data.City)
	if err != nil {
		return
	}

	err = b.Fill(SELECTOR_REGISTER_COUNTRY_SELECT, data.Country)
	if err != nil {
		return
	}

	err = b.Click(SELECTOR_REGISTER_REGISTER_BTN)
	if err != nil {
		return
	}

	err = b.WaitForLabels(func(labels botty.Labels) bool {
		return labels.Has(PAGE_ACCOUNT)
	}, 2*time.Second, 100*time.Millisecond)
	if err != nil {
		return
	}

	return
}

func main() {
	err := run()
	if err != nil {
		log.Fatal(err)
	}
}

func run() (err error) {
	reporter := botty.ConsoleReporter{}

	browser, err := botty.NewBrowser("http://192.168.99.100:4444/wd/hub", "chrome")
	if err != nil {
		return
	}

	bot := NewShopwareBot(browser, reporter)
	defer bot.Close()

	email := fake.EmailAddress()
	password := fake.Password(8, 12, true, true, false)

	data := RegistrationData{
		FirstName: fake.FirstName(),
		LastName:  fake.LastName(),
		Email:     email,
		Password:  password,
		Street:    fake.Street(),
		ZipCode:   fake.Zip(),
		City:      fake.City(),
		Country:   "Deutschland",
	}

	println(email)
	println(password)

	err = bot.Register(data)
	if err != nil {
		return
	}

	bot.Browser.Screenshot("/tmp/ebay.jpg")

	return
}
