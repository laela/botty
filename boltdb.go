package botty

import (
	"encoding/json"
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"net/http"
	"gitlab.com/laela/botty"
)

func NewBoltdbStore(file string, cookiesBucket string) BoltdbStore {
	return BoltdbStore{
		file:           file,
		cookiesBucket: cookiesBucket,
	}
}

type BoltdbStore struct {
	file           string
	cookiesBucket string
}

func (datastore BoltdbStore) SetCookies(username string, cookies []*http.Cookie) error {
	database, err := bolt.Open(datastore.file, 0600, nil)
	if err != nil {
		return err
	}
	defer database.Close()

	err = database.Update(func(transaction *bolt.Tx) error {
		_, err := transaction.CreateBucketIfNotExists([]byte(datastore.cookiesBucket))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		return nil
	})
	if err != nil {
		return err
	}

	serialized, err := json.Marshal(cookies)
	if err != nil {
		return err
	}

	err = database.Update(func(transaction *bolt.Tx) error {
		b := transaction.Bucket([]byte(datastore.cookiesBucket))
		err := b.Put([]byte(username), serialized)
		return err
	})
	if err != nil {
		return err
	}

	return nil
}

func (datastore BoltdbStore) GetCookies(username string) ([]*http.Cookie, error) {
	database, err := bolt.Open(datastore.file, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer database.Close()

	err = database.Update(func(transaction *bolt.Tx) error {
		_, err := transaction.CreateBucketIfNotExists([]byte(datastore.cookiesBucket))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	serialized := []byte("")

	err = database.View(func(transaction *bolt.Tx) error {
		bucket := transaction.Bucket([]byte(datastore.cookiesBucket))
		serialized = bucket.Get([]byte(username))

		return nil
	})
	if err != nil {
		return nil, err
	}

	cookies := []*http.Cookie{}

	if len(serialized) == 0 {
		return nil, &botty.CookiesNotFound{}
	}

	err = json.Unmarshal(serialized, &cookies)
	if err != nil {
		return nil, err
	}

	return cookies, nil
}
